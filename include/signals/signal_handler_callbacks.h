#ifndef SIGNAL_HANDLER_CALLBACKS_H_
#define SIGNAL_HANDLER_CALLBACKS_H_

#include "siginfo.h"

#include <unistd.h>

namespace signals
{
    class SYMEXPORT SignalHandlerCallbacks {
    public:
        virtual ~SignalHandlerCallbacks() = default;

        virtual bool onSignal(const SigInfo& sigInfo) = 0;
    };
} /* namespace signals */

#endif /* SIGNAL_HANDLER_CALLBACKS_H_ */

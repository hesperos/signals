#include "signals/factory.h"
#include "signals/sigwait_handler.h"
#include "signals/signalfd_handler.h"

namespace signals
{

    SignalHandlerFactory::SignalHandlerFactory() :
        ops(std::make_shared<RealFdOps>())
    {
    }

    std::unique_ptr<SignalHandler>
        SignalHandlerFactory::createSignalFdHandler(
                const ::sigset_t& sigSet,
                std::unique_ptr<SignalHandlerCallbacks> cb) {
            return std::make_unique<SignalFdHandler>(sigSet, std::move(cb), ops);
        }

    std::unique_ptr<SignalHandler>
        SignalHandlerFactory::createSigWaitHandler(
                const ::sigset_t& sigSet,
                std::unique_ptr<SignalHandlerCallbacks> cb) {
            return std::make_unique<SigWaitHandler>(sigSet, std::move(cb));
        }

} /* namespace  signals */

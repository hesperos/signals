#include "signals/signal_handler.h"

namespace signals
{
    SignalHandler::SignalHandler(std::unique_ptr<SignalHandlerCallbacks> cb) :
        callbacks{std::move(cb)}
    {
    }

    SignalHandler::~SignalHandler() {
        stop();
    }

    void SignalHandler::stop() {
        if (isRunning.load()) {
            isRunning.store(false);
            stopImpl();
        }
    }

    void SignalHandler::run() {
        isRunning.store(true);
        while (isRunning.load()) {
            if (runImpl()) {
                break;
            }
        }
    }

} /* namespace signals */

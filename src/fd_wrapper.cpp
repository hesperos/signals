#include "signals/fd_wrapper.h"

#include <stdexcept>
#include <cstring>
#include <cerrno>

namespace signals
{
    FdWrapper::FdWrapper(int fd,
            const std::string& name,
            std::shared_ptr<FdOps> ops) :
        fd{fd, ops},
        name{name}
    {
        if (!this->fd.isValid()) {
            const std::string errStr = ::strerror(errno);
            throw std::runtime_error(name + ": " + errStr);
        }
    }

    int FdWrapper::get() const noexcept {
        return fd.get();
    }
} /* namespace signals */

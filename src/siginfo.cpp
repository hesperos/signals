#include "signals/siginfo.h"

namespace signals {

SigInfo SigInfo::fromSigInfo(const ::signalfd_siginfo &si) {
  return {
      static_cast<int>(si.ssi_signo),
      static_cast<::pid_t>(si.ssi_pid),
      static_cast<int>(si.ssi_int),
      reinterpret_cast<void *>(si.ssi_ptr),
  };
}

SigInfo SigInfo::fromSigInfo(const ::siginfo_t &si) {
  return {
      si.si_signo,
      si.si_pid,
      si.si_int,
      si.si_ptr,
  };
}

} // namespace signals

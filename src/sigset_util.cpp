#include "signals/sigset_util.h"

namespace signals
{
    ::sigset_t createFullSet() noexcept {
        ::sigset_t set;
        sigfillset(&set);
        return set;
    }
} /* namespace signals */

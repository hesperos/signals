#include "signals/scoped_fd.h"

#include <unistd.h>

#include <utility>

namespace signals
{
    void RealFdOps::close(int fd) {
        ::close(fd);
    }

    ScopedFd::ScopedFd(int fd, std::shared_ptr<FdOps> ops) :
        fd{fd},
        ops{ops}
    {
    }

    ScopedFd::ScopedFd(ScopedFd&& o) :
        fd{o.fd},
        ops{o.ops}
    {
        o.fd = InvalidFd;
    }

    ScopedFd& ScopedFd::operator=(ScopedFd&& o) {
        ScopedFd tmp = std::move(*this);
        std::swap(fd, o.fd);
        return *this;
    }

    ScopedFd::~ScopedFd() {
        if (fd != InvalidFd) {
            ops->close(fd);
        }
    }

    int ScopedFd::get() const noexcept {
        return fd;
    }

    bool ScopedFd::isValid() const noexcept {
        return fd != InvalidFd;
    }

} /* namespace signals */

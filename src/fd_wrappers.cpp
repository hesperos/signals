#include "signals/fd_wrappers.h"

#include <sys/signalfd.h>
#include <sys/eventfd.h>
#include <sys/epoll.h>
#include <unistd.h>

#include <cstring>
#include <cerrno>
#include <stdexcept>

namespace signals
{
    SignalFd::SignalFd(const ::sigset_t &sigSet, std::shared_ptr<FdOps> ops) : FdWrapper(signalfd(-1, &sigSet, SFD_CLOEXEC), "signalfd", ops)
    {
    }

    SigInfo SignalFd::wait()
    {
        signalfd_siginfo sigInfo;
        if (ssize_t s = ::read(fd.get(), &sigInfo, sizeof(sigInfo)); s < 0) {
            const auto err = errno;
            throw std::runtime_error(
                std::string("unable to read signal information: ") +
                ::strerror(err));
        }

        return SigInfo::fromSigInfo(sigInfo);
    }

    EventFd::EventFd(std::shared_ptr<FdOps> ops) : FdWrapper(eventfd(0, EFD_CLOEXEC), "eventfd", ops)
    {
    }

    void EventFd::notify() noexcept
    {
        uint64_t u = 0;
        ::write(get(), &u, sizeof(u));
    }

    void EventFd::wait()
    {
        uint64_t u = 0;
        if (ssize_t n = ::read(get(), &u, sizeof(u)); n != sizeof(uint64_t))
        {
            throw std::runtime_error("expected to read an event of size uint64");
        }
    }

    EpollFd::EpollFd(std::shared_ptr<FdOps> ops) : FdWrapper(epoll_create1(EPOLL_CLOEXEC), "epollfd", ops)
    {
    }

    void EpollFd::addEventSource(const FdWrapper &fdw)
    {
        struct epoll_event ev;
        ev.events = EPOLLIN;
        ev.data.fd = fdw.get();
        if (-1 == epoll_ctl(fd.get(), EPOLL_CTL_ADD, ev.data.fd, &ev))
        {
            throw std::runtime_error("unable to add event source");
        }
    }

    std::vector<int> EpollFd::wait()
    {
        const std::size_t maxEvents = 16;
        struct epoll_event events[maxEvents];
        const int nfds = epoll_wait(fd.get(), events, maxEvents, -1);
        if (-1 == nfds)
        {
            const auto err = errno;
            throw std::runtime_error(
                std::string("error waiting for an event: ") + ::strerror(err));
        }

        std::vector<int> fds{};
        for (ssize_t i = 0; i < nfds; ++i)
        {
            if (events[i].events & EPOLLIN)
            {
                fds.push_back(events[i].data.fd);
            }
        }

        return fds;
    }

} /* namespace signals */

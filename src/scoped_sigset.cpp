#include "signals/scoped_sigset.h"

#include <errno.h>

#include <cstring>
#include <string>
#include <stdexcept>

namespace
{
    ::sigset_t setSigMask(const ::sigset_t& newSet) {
        ::sigset_t oldSet;
        if (0 != ::pthread_sigmask(SIG_SETMASK, &newSet, &oldSet)) {
            const std::string errStr = ::strerror(errno);
            throw std::runtime_error(errStr);
        }
        return oldSet;
    }
} /* namespace  */

namespace signals
{
    ScopedSigSet::ScopedSigSet(const ::sigset_t& sigSet) :
        oldSet{setSigMask(sigSet)}
    {
    }

    ScopedSigSet::~ScopedSigSet() {
        setSigMask(oldSet);
    }
} /* namespace signals */

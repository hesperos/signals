#include <gmock/gmock.h>

#include "signals/fd_wrapper.h"

#include "mock/mock_fd_ops.h"

#include <memory>

using namespace ::testing;

class FdWrapperTest : public ::testing::Test {
public:
    void SetUp() {
        fdOps = std::make_shared<MockFdOps>();
    }

    void TearDown() {
        fdOps.reset();
    }

protected:
    std::shared_ptr<MockFdOps> fdOps;
};

TEST_F(FdWrapperTest, testIfThrowsOnInvalidFd) {
    EXPECT_CALL(*fdOps, close(_))
        .Times(Exactly(0));

    ASSERT_THROW(signals::FdWrapper(-1, "incorrect fd", fdOps), std::exception);
}

TEST_F(FdWrapperTest, testIfReturnsFdCorrectly) {
    const int fdNo = 123;
    EXPECT_CALL(*fdOps, close(fdNo))
        .Times(Exactly(1));

    signals::FdWrapper fd(fdNo, "fake", fdOps);
    ASSERT_EQ(fd.get(), fdNo);
}
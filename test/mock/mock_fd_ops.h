#ifndef MOCK_FD_OPS_H_
#define MOCK_FD_OPS_H_

#include <gmock/gmock.h>

#include "signals/scoped_fd.h"

class MockFdOps : public signals::FdOps {
public:
    MOCK_METHOD1(close, void(int));
};

#endif // MOCK_FD_OPS_H_